import "./index.css";
import linkedin from "../../assets/linkedin.svg";
import gitlab from "../../assets/gitlab.svg";
import discord from "../../assets/discord.svg";
import github from "../../assets/github.svg";

import { ToolTip } from "../ToolTip";
import { useState } from "react";

const Footer = (props: any) => {
  const { pageType } = props;
  const [isLIHovered, setIsLIHovered] = useState(false);
  const [isGitHovered, setIsGitHovered] = useState(false);
  const [isGitHubHovered, setIsGitHubHovered] = useState(false);

  const onHover = (icon: string) => {
    if (icon === "linkedin") setIsLIHovered(true);
    if (icon === "gitlab") setIsGitHovered(true);
    if (icon === "github") setIsGitHubHovered(true);
  };

  const onLeave = (icon: string) => {
    if (icon === "linkedin") setIsLIHovered(false);
    if (icon === "gitlab") setIsGitHovered(false);
    if (icon === "github") setIsGitHubHovered(false);
  };

  return (
    <div className="footer">
      <div className="icon-menu">
        <div className={`icon-${isLIHovered}`}>
          <img
            src={linkedin}
            alt="linkedin"
            onClick={() =>
              window.open("https://www.linkedin.com/in/jena-adkins/", "_blank")
            }
            onMouseOver={() => onHover("linkedin")}
            onMouseLeave={() => onLeave("linkedin")}
          />
        </div>
        <div className={`icon-${isGitHovered}`}>
          <img
            src={gitlab}
            alt="gitlab"
            onClick={() =>
              window.open("https://gitlab.com/jenaadkins", "_blank")
            }
            onMouseOver={() => onHover("gitlab")}
            onMouseLeave={() => onLeave("gitlab")}
          />
        </div>
        <div className={`icon-${isGitHubHovered}`}>
          <img
            src={github}
            alt="github"
            onClick={() => window.open("https://github.com/jenawen", "_blank")}
            onMouseOver={() => onHover("github")}
            onMouseLeave={() => onLeave("github")}
          />
        </div>
        <div>
          {" "}
          {pageType === "mobile" ? (
            <>
              {" "}
              <ToolTip displayText={"jenawen#0"} position={"left"}>
                <img src={discord} alt="discord" />
              </ToolTip>
            </>
          ) : (
            <>
              {" "}
              <ToolTip displayText={"jenawen#0"}>
                <img src={discord} alt="discord" />
              </ToolTip>
            </>
          )}
        </div>
      </div>
      <div className="cw">
        Designed & developed with love by Jena Adkins. 2023.
      </div>
    </div>
  );
};

export default Footer;
