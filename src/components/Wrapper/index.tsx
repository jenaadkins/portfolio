import { useEffect, useState } from "react";
import SideBar from "../SideBar/index";
import MainContent from "../MainContent";

import "./index.css";
import Footer from "../Footer";
import PortfolioContents from "../MainContent/Portfolio/PortfolioContents";

const Wrapper = () => {
  const [portfolioActive, setPortfolioActive] = useState<boolean>(false);
  const [portfolioPage, setPortfolioPage] = useState<string>("none");
  const [pageType, setPageType] = useState("desktop");
  const width = window.innerWidth;

  useEffect(() => {
    if (width <= 480) {
      setPageType("mobile");
    }
  }, [width]);

  return (
    <>
      {pageType === "mobile" ? (
        <div className="mob-wrapper">
          <div className="backing">
            {portfolioActive ? (
              <PortfolioContents
                setPortfolioActive={setPortfolioActive}
                portfolioPage={portfolioPage}
                setPortfolioPage={setPortfolioPage}
                pageType={pageType}
              />
            ) : (
              <div className="wrapper">
                <div>
                  <MainContent
                    pageType={pageType}
                    setPortfolioActive={setPortfolioActive}
                    setPortfolioPage={setPortfolioPage}
                  />
                </div>
              </div>
            )}
          </div>
          <Footer pageType={pageType} />
        </div>
      ) : (
        <>
          <div className="backing">
            {portfolioActive ? (
              <PortfolioContents
                setPortfolioActive={setPortfolioActive}
                portfolioPage={portfolioPage}
                setPortfolioPage={setPortfolioPage}
              />
            ) : (
              <div className="wrapper">
                <div>
                  <SideBar />
                </div>
                <div>
                  <MainContent
                    pageType={pageType}
                    setPortfolioActive={setPortfolioActive}
                    setPortfolioPage={setPortfolioPage}
                  />
                </div>
              </div>
            )}
          </div>
          <Footer />
        </>
      )}
    </>
  );
};

export default Wrapper;
