import { useEffect, useState } from "react";
import Portfolio from "./Portfolio";
import Biography from "./Biography";
import Skills from "./Skills";
import "./index.css";
import Contact from "./Contact";
import SideBar from "../SideBar";

const MainContent = (props: any) => {
  const { setPortfolioActive, setPortfolioPage, pageType } = props;
  const [current, setCurrent] = useState(1);
  const [isHovered, setIsHovered] = useState(false);
  const [page, setPage] = useState("hi");
  // const [isActive, setisActive] = useState(false);

  const onHover = (text: string) => {
    setIsHovered(true);
    setPage(text);
  };

  const onLeave = () => {
    setIsHovered(false);
  };

  const handleOnClick = (btnNumber: number) => {
    setCurrent(btnNumber);
  };

  useEffect(() => {
    if (current === 6) {
      setCurrent(1);
    }
    if (document) {
      document
        .querySelector(`#slide-${current > 6 ? 1 : current}`)!
        .scrollIntoView();
    }
  }, [current]);

  return (
    <>
      {pageType === "mobile" ? (
        <div>
          {" "}
          {isHovered && <div className={`hover-text-${isHovered}`}>{page}</div>}
          <div className="tabs">
            {/* <ul className="tab-bar">
              <button
                // onClick={() => handleTab2()}
                onClick={() => handleOnClick(2)}
                onMouseOver={() => {
                  onHover("portfolio");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab3()}
                onClick={() => handleOnClick(3)}
                onMouseOver={() => {
                  onHover("about me");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab4()}
                onClick={() => handleOnClick(4)}
                onMouseOver={() => {
                  onHover("skill set");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true}`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab4()}
                onClick={() => handleOnClick(5)}
                onMouseOver={() => {
                  onHover("contact me");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
            </ul> */}
            <div className="content">
              <div className="intro" id="slide-1">
                <SideBar />
                <h1> ₊‧°𐐪♡𐑂°‧₊</h1>
                <p>
                  {" "}
                  <p>
                    {" "}
                    <h2> Hello! I'm Jena.</h2>
                  </p>
                  I'm a front-end developer and undergraduate student majoring
                  in Computer Science at UNLV.{" "}
                </p>
                <p>
                  Currently working as a Web Development Intern at{" "}
                  <a href="https://www.creditonebank.com/">Credit One Bank</a>.
                </p>
              </div>
              <div className="page" id="slide-2">
                {" "}
                <Portfolio
                  setPortfolioActive={setPortfolioActive}
                  setPortfolioPage={setPortfolioPage}
                  pageType={pageType}
                />{" "}
              </div>
              <div className="page" id="slide-3">
                {" "}
                <Biography />{" "}
              </div>
              <div className="page" id="slide-4">
                {" "}
                <Skills />{" "}
              </div>
              <div className="page" id="slide-5">
                {" "}
                <Contact />{" "}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div>
          {" "}
          {isHovered && <div className={`hover-text-${isHovered}`}>{page}</div>}
          <div className="tabs">
            <ul className="tab-bar">
              {/* <li
          className={isActive === "tab1" ? "active" : ""}
          onClick={() => handleTab1()}
        >
          {" "}
          ♡
        </li> */}
              <button
                // onClick={() => handleTab2()}
                onClick={() => handleOnClick(2)}
                onMouseOver={() => {
                  onHover("portfolio");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab3()}
                onClick={() => handleOnClick(3)}
                onMouseOver={() => {
                  onHover("about me");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab4()}
                onClick={() => handleOnClick(4)}
                onMouseOver={() => {
                  onHover("skill set");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true}`}
              >
                {" "}
                ♥
              </button>
              <button
                // onClick={() => handleTab4()}
                onClick={() => handleOnClick(5)}
                onMouseOver={() => {
                  onHover("contact me");
                }}
                onMouseLeave={() => {
                  onLeave();
                }}
                className={`btn btn-true`}
              >
                {" "}
                ♥
              </button>
            </ul>
            <div className="content">
              <div className="intro" id="slide-1">
                {" "}
                <h1> ₊‧°𐐪♡𐑂°‧₊</h1>
                <p>
                  {" "}
                  <p>
                    {" "}
                    <h2> Hello! I'm Jena.</h2>
                  </p>
                  I'm a front-end developer and undergraduate student majoring
                  in Computer Science at UNLV.{" "}
                </p>
                <p>
                  Currently working as a Web Development Intern at{" "}
                  <a href="https://www.creditonebank.com/">Credit One Bank</a>
                </p>
              </div>
              <div className="page" id="slide-2">
                {" "}
                <Portfolio
                  setPortfolioActive={setPortfolioActive}
                  setPortfolioPage={setPortfolioPage}
                />{" "}
              </div>
              <div className="page" id="slide-3">
                {" "}
                <Biography />{" "}
              </div>
              <div className="page" id="slide-4">
                {" "}
                <Skills />{" "}
              </div>
              <div className="page" id="slide-5">
                {" "}
                <Contact />{" "}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default MainContent;
