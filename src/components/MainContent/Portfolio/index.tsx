import "./index.css";
const Portfolio = (props: any) => {
  const { setPortfolioActive, setPortfolioPage, pageType } = props;
  return (
    <div>
      <h1>portfolio</h1>
      {/* <div className="proj-header">work projects</div> */}

      {pageType === "mobile" ? (
        <div>
          {" "}
          <div className="row2">
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("RegZ");
                  setPortfolioActive(true);
                }}
              >
                Reg Z Calculator{" "}
              </button>
            </div>
            <div className="proj">
              {" "}
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("R246");
                  setPortfolioActive(true);
                }}
              >
                Underwriting
              </button>
            </div>
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("DB");
                  setPortfolioActive(true);
                }}
              >
                Digital Bank
              </button>
            </div>
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("GJ");
                  setPortfolioActive(true);
                }}
              >
                Glucose Journal
              </button>
            </div>
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("P");
                  setPortfolioActive(true);
                }}
              >
                Portfolio
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div>
          {" "}
          <div className="row2">
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("RegZ");
                  setPortfolioActive(true);
                }}
              >
                Reg Z Calculator{" "}
              </button>
            </div>
            <div className="proj">
              {" "}
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("R246");
                  setPortfolioActive(true);
                }}
              >
                Underwriting
              </button>
            </div>
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("DB");
                  setPortfolioActive(true);
                }}
              >
                Digital Bank
              </button>
            </div>
          </div>
          {/* <div className="proj-header">personal projects</div> */}
          <div className="row2">
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("GJ");
                  setPortfolioActive(true);
                }}
              >
                Glucose Journal
              </button>
            </div>
            <div className="proj">
              <button
                className="p-btn"
                onClick={() => {
                  setPortfolioPage("P");
                  setPortfolioActive(true);
                }}
              >
                Personal Website
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Portfolio;
