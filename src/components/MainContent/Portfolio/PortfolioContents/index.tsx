import DigitalBank from "./DigitalBank";
import GlucoseJournal from "./GlucoseJournal";
import leftsmall from "../../../../assets/leftsmall.svg";
import RegZ from "./RegZ";
import "./index.css";
import Underwriting from "./Underwriting";
import MyPortfolio from "./MyPortfolio";
const PortfolioContents = (props: any) => {
  const { setPortfolioActive, portfolioPage } = props;

  return (
    <>
      <div onClick={() => setPortfolioActive(false)} className="back-btn">
        <img src={leftsmall} /> &nbsp;
        <span>Back</span>
      </div>
      <div className="port-contents">
        {portfolioPage === "RegZ" && <RegZ />}
        {portfolioPage === "R246" && <Underwriting />}
        {portfolioPage === "DB" && <DigitalBank />}
        {portfolioPage === "GJ" && <GlucoseJournal />}
        {portfolioPage === "P" && <MyPortfolio />}
      </div>
    </>
  );
};
export default PortfolioContents;
