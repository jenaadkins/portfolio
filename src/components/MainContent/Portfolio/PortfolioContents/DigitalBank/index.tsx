import Pill from "../../../../Pill";
import "../index.css";
import cone from "../../../../../assets/cone.svg";

const DigitalBank = () => {
  return (
    <div>
      <div className="header">
        Digital Bank <p>2023</p>
      </div>

      <div className="contents">
        <div className="desc">
          {" "}
          <p>An application for customer savings and checkings accounts.</p>
          <p>
            Due to this being an ongoing project, I can only provide sparse
            details about it. However, it's a very high-profile project for the
            current company.
          </p>
          <p>Participated in: Custom form creation, Unit testing</p>
          <p>
            Current status: <Pill text={"In-progress"} status={"inprogress"} />{" "}
          </p>
          <p>
            {" "}
            <Pill
              text={"React"}
              link={"https://react.dev/"}
              status={"def"}
            />{" "}
            <Pill
              text={"TypeScript"}
              link={"https://www.typescriptlang.org/"}
              status={"def"}
            />{" "}
            <Pill text={"Jest"} link={"https://jestjs.io/"} status={"def"} />
          </p>
        </div>
        <div className="photo-container">
          <div className="cone">
            {" "}
            <img src={cone} />{" "}
          </div>
          This page is under construction. Pictures will be added soon!
        </div>
      </div>
    </div>
  );
};

export default DigitalBank;
