import Pill from "../../../../Pill";
import "../index.css";
import cone from "../../../../../assets/cone.svg";

const RegZ = () => {
  return (
    <div>
      <div className="header">
        Regulation Z<p>2021-2022</p>
      </div>

      <div className="contents">
        <div className="desc">
          {" "}
          <p>
            {" "}
            A Regulation Z calculator that calculates the amount of credit due
            from purchases, cash advance fees, or merchant charges.
          </p>
          <p>
            A credit protection feature is also included for credit statements.
          </p>
          <p>
            This was my first project as an intern. Our task was to create an
            interface to replace an Excel sheet that had the same functions, but
            was becoming increasingly obsolete and difficult to update.
          </p>
          <p>Participated in: Front-end, Unit Testing</p>
          <p>
            Current status: <Pill text={"Deployed"} status={"deployed"} />{" "}
          </p>
          <p>
            <Pill text={"React"} link={"https://react.dev/"} status={"def"} />{" "}
            <Pill
              text={"TypeScript"}
              link={"https://www.typescriptlang.org/"}
              status={"def"}
            />{" "}
            <Pill
              text={"HTML"}
              link={"https://developer.mozilla.org/en-US/docs/Glossary/HTML5"}
              status={"def"}
            />{" "}
            <Pill
              text={"SASS"}
              link={"https://sass-lang.com/documentation/"}
              status={"def"}
            />{" "}
            <Pill text={"Jest"} link={"https://jestjs.io/"} status={"def"} />
          </p>
        </div>
        <div className="photo-container">
          <div className="cone">
            {" "}
            <img src={cone} />{" "}
          </div>
          This page is under construction. Pictures will be added soon!
        </div>
      </div>
    </div>
  );
};

export default RegZ;
