import Pill from "../../../../Pill";
import "../index.css";

import port1 from "../../../../../assets/portfolioimages/port1.png";
import port2 from "../../../../../assets/portfolioimages/port2.png";

const MyPortfolio = () => {
  return (
    <div>
      <div className="header">
        Personal Website & Portfolio <p>2023</p>
      </div>

      <div className="contents">
        <div className="desc">
          {" "}
          <p>
            My personal website and portfolio to display my professional work
            and personality. (This current page!)
          </p>
          <p>
            When developing this interface, I wanted to showcase myself as
            genuinely as possible. I chose colors I love and wanted to
            incorporate design aspects that are soft and less intense.
          </p>
          <p>
            Overall I wanted an aesthetic that represented me, even if it's not
            conventional or widely seen. For this project, I wanted to
            incorporate things I liked, not what I thought other people would
            like or would approve of.
          </p>
          <p>
            Current status: <Pill text={"Maintenance"} status={"maintain"} />
          </p>
          <p>
            {" "}
            <Pill
              text={"Vite"}
              link={"https://vitejs.dev/"}
              status={"def"}
            />{" "}
            <Pill text={"React"} link={"https://react.dev/"} status={"def"} />{" "}
            <Pill
              text={"TypeScript"}
              link={"https://www.typescriptlang.org/"}
              status={"def"}
            />{" "}
            <Pill
              text={"HeroTofu"}
              link={"https://herotofu.com/"}
              status={"def"}
            />
            <Pill
              text={"Gitlab Pages"}
              link={"https://docs.gitlab.com/ee/user/project/pages/"}
              status={"def"}
            />
          </p>
        </div>
        <div className="contain">
          <div className="photo-container">
            <img src={port1} />
            <p></p>
            <img src={port2} />
          </div>
          <div className="caption">Preliminary design sketches</div>
        </div>
      </div>
    </div>
  );
};

export default MyPortfolio;
