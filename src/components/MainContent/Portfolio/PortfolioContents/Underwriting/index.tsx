import Pill from "../../../../Pill";
import "../index.css";
import uw1 from "../../../../../assets/portfolioimages/uw1.png";
import uw3 from "../../../../../assets/portfolioimages/uw3.png";
const Underwriting = () => {
  return (
    <div>
      <div className="header">
        Underwriting (RISK-246)
        <p>2021-2022</p>
      </div>

      <div className="contents">
        <div className="desc">
          {" "}
          <p>
            {" "}
            An underwriting application that allows the user to create a custom
            credit line for a specific credit card.
          </p>
          <p>
            {" "}
            This was one page of a larger internal project titled RISK-246. The
            purpose of the project was to create, edit, and manage credit card
            campaigns more efficiently.{" "}
          </p>
          <p> Participated in: Front-end, Unit Testing, and Documentation</p>
          <p>
            Current status: <Pill text={"Deployed"} status={"deployed"} />{" "}
          </p>
          <p>
            {" "}
            <Pill
              text={"React"}
              link={"https://react.dev/"}
              status={"def"}
            />{" "}
            <Pill
              text={"TypeScript"}
              link={"https://www.typescriptlang.org/"}
              status={"def"}
            />{" "}
            <Pill
              text={"HTML"}
              link={"https://developer.mozilla.org/en-US/docs/Glossary/HTML5"}
              status={"def"}
            />{" "}
            <Pill
              text={"SASS"}
              link={"https://sass-lang.com/documentation/"}
              status={"def"}
            />{" "}
            <Pill text={"Jest"} link={"https://jestjs.io/"} status={"def"} />
          </p>
        </div>
        <div className="photo-container">
          <img src={uw1} />
          <div className="caption">
            Example credit line in the Underwriting page
          </div>
          <p></p>
          <img src={uw3} />

          <div className="caption">
            Customizable underwriting form interface
          </div>
        </div>
      </div>
    </div>
  );
};

export default Underwriting;
