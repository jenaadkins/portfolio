import Pill from "../../../../Pill";
import "../index.css";
import journal from "../../../../../assets/portfolioimages/journal.png";

const GlucoseJournal = () => {
  return (
    <div>
      <div className="header">
        Food & Glucose Journal
        <p>2023</p>
      </div>

      <div className="contents">
        <div className="desc">
          {" "}
          <p>
            {" "}
            A simple journal-like application to track food and glucose levels.
            The glucose level graph allows the user to visually see their
            progress over time.
          </p>
          <p>
            I created this project for my partner who had extremely high blood
            sugar levels and was diagnosed with diabetes by his doctor. For a
            month, we wanted to track his food intake and glucose levels. I was
            using my notes app in my phone.
          </p>
          <p>
            I got frustrated with the notes interface and decided to make my own
            instead!
          </p>
          <p>
            Current status: <Pill text={"In-progress"} status={"inprogress"} />{" "}
          </p>
          <p>
            {" "}
            <Pill
              text={"React"}
              link={"https://react.dev/"}
              status={"def"}
            />{" "}
            <Pill
              text={"TypeScript"}
              link={"https://www.typescriptlang.org/"}
              status={"def"}
            />{" "}
            <Pill
              text={"CSS"}
              link={"https://developer.mozilla.org/en-US/docs/Web/CSS"}
              status={"def"}
            />
            <Pill
              text={"Chart.js"}
              link={"https://www.chartjs.org/docs/latest/"}
              status={"def"}
            />
          </p>
        </div>
        <div className="photo-container">
          <img src={journal} />

          <div className="caption">Glucose level graph made using Chart.js</div>
        </div>
      </div>
    </div>
  );
};

export default GlucoseJournal;
