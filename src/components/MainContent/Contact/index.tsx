import { useEffect, useState } from "react";
import "./index.css";
import envopen from "../../../assets/envopen.svg";
import envclosed from "../../../assets/envclosed.svg";

const Contact = () => {
  const form_ep =
    "https://public.herotofu.com/v1/56b70060-178f-11ee-8025-97a9fb2f29da";
  const [submitted, setSubmitted] = useState(false);
  const [activeImg, setActiveImg] = useState(envopen);
  const [sent, setIsSent] = useState("Send!");
  const handleSubmit = (e: any) => {
    e.preventDefault();

    const inputs = e.target.elements;
    const data: any = {};

    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].name) {
        data[inputs[i].name] = inputs[i].value;
      }
    }

    fetch(form_ep, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Form response was not ok");
        }
        setIsSent("Message received!");
        setActiveImg(envclosed);
        setSubmitted(true);
      })
      .catch((err) => {
        // Submit the form manually
        e.target.submit();
        console.warn(err);
      });
  };

  useEffect(() => {
    if (submitted) {
      setTimeout(() => {
        setActiveImg(envopen);
        setSubmitted(false);
        setIsSent("Send!");
      }, 15000);
    }
  }, [submitted]);

  return (
    <div className="text-container">
      <h1>contact me</h1>
      <p>If you have any questions, or just want to chat, contact me here.</p>
      <p>I'll get back to you as soon as I can!</p>
      <form action={form_ep} onSubmit={handleSubmit} method="POST">
        <div className="inputs">
          <input type="text" placeholder="Your name" name="name" required />
        </div>
        <div>
          <input type="email" placeholder="Email" name="email" />
        </div>
        <div>
          <textarea placeholder="Your message" name="message" required />
        </div>
        <div className="send">
          <button type="submit" className="send">
            <img src={activeImg} />
            {sent}
          </button>
        </div>
      </form>
    </div>
  );
};

export default Contact;
