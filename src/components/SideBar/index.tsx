import "./index.css";

import me from "../../assets/me.png";
import "../../fonts/Cherolina.ttf";

const SideBar = () => {
  //!fix title idk where it   should go lol
  return (
    <>
      <div className="title-name"> Jena Adkins</div>

      <div className="sidebar">
        <div className="picture">
          <img src={me} />
        </div>
        <div className="links">&nbsp;</div>
      </div>
    </>
  );
};

export default SideBar;
